package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Особые случаи
        if(inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        if(inputNumbers.size() >= Integer.MAX_VALUE-1) throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);
        if(inputNumbers.size()==1) {
            int[][] res = new int[][]{{1}};
            return res;
        }
        // Определим когда пожно построить пирамиду
        boolean canBuild = false;
        Integer prefSize = 0;
        int rows = 0;
        int cols = 0;
        for(int i = 1; i <= inputNumbers.size(); i++) {
            if(prefSize>inputNumbers.size()) {
                break;
            }
            if(inputNumbers.size()==prefSize) {
                canBuild = true;
                break;
            }
            prefSize+=i;
            rows = i;
            cols = 2*i-1;
        }
        if(!canBuild){
            throw new CannotBuildPyramidException();
        }
        int[][] res = new int[rows][cols];
        // Построение пирамиды
        // Позиция вставки первого элемента, далее на 1 меньше
        int pos = rows-1;
        // Количество вставляемых элементов
        int count = 1;
        // Какой элемент вставляем
        int k = 0;
        for(int i = 0; i < rows; i++) {
            int currentPos = pos;
            for(int j = 0; j < count; j++) {
                res[i][currentPos] = inputNumbers.get(k);
                k++;
                currentPos+=2;
            }
            count++;
            pos--;
        }
        return res;
    }

}
