package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y){
        // Особый случай!
        if(x==null||y==null) {
            throw new IllegalArgumentException("they are null");
        }
        // Проверяем порядок следования элементов из Х в У
        // Позичия по списку Y
        int startIndex = 0;
        // Есть ли текущий Х в мн-ве У
        boolean isInY;
        for(int i = 0; i < x.size(); i++) {
            isInY = false;
            for(int j = startIndex; j < y.size(); j++) {
                if((x.get(i)).equals(y.get(j))) {
                    startIndex = j;
                    isInY = true;
                    break;
                }
            }
            if(!isInY){
                return false;
            }
        }
        return true;
    }
}
