package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result = null;
        Helper h = new Helper();
        try{
            h.parse(statement);
            result = h.counting();
            // Форматирование вывода
            if(Math.abs(Double.parseDouble(result)-Math.round(Double.parseDouble(result)))<0.001) {
                result = String.valueOf((int)Double.parseDouble(result));
            }else{
                result = new DecimalFormat("#0.0000").format(Double.parseDouble(result));
                result = result.replace(',','.');
                result = String.valueOf(Double.parseDouble(result));
            }
        }catch (Exception e) {
            return null;
        }
        System.out.println(result);
        return result;
    }
}
