package com.tsystems.javaschool.tasks.calculator;

import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Created by Mikhail on 19.01.2018.
 */
public class Helper {
    // list of available functions
    private final String OPERATORS = "+-*/";
    // separator of arguments
    // temporary stack that holds operators, functions and brackets
    private Stack<String> stackOperations = new Stack<>();
    // stack for holding expression converted to reversed polish notation
    private Stack<String> stackOPN = new Stack<>();
    // stack for holding the calculations result
    private Stack<String> stackAnswer = new Stack<>();

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    private byte getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) {
            return 1;
        }
        return 2;
    }

    public void parse(String expression) throws Exception{
        // чистка стэка перед преобразованием в обратную польскую нотацию
        stackOperations.clear();
        stackOPN.clear();

        // преобразование исходной строки, решение проблемы с минусами перед числами
        expression = expression.replace(" ", "").replace("(-", "(0-");
        if (expression.length()>0 && expression.charAt(0) == '-') {
            expression = "0" + expression;
        }
        // токенизация
        StringTokenizer stringTokenizer = new StringTokenizer(expression,
                OPERATORS + "()", true);

        // алгоритм Дейкстры для получения из инфиксной ОПН
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isOpenBracket(token)) {
                stackOperations.push(token);
            } else if (isCloseBracket(token)) {
                while (!stackOperations.empty()
                        && !isOpenBracket(stackOperations.lastElement())) {
                    stackOPN.push(stackOperations.pop());
                }
                stackOperations.pop();
            } else if (isNumber(token)) {
                stackOPN.push(token);
            } else if (isOperator(token)) {
                while (!stackOperations.empty()
                        && isOperator(stackOperations.lastElement())
                        && getPrecedence(token) <= getPrecedence(stackOperations
                        .lastElement())) {
                    stackOPN.push(stackOperations.pop());
                }
                stackOperations.push(token);
            }
        }

        while (!stackOperations.empty()) {
            stackOPN.push(stackOperations.pop());
        }

        Collections.reverse(stackOPN);
    }

    public String counting() throws EmptyStackException{
        Stack<String> stackPN = stackOPN;
        if(stackPN.contains(")") || stackPN.contains("(")) return null;
        while(!stackPN.empty()) {
            if(isNumber(stackPN.peek())) {
                stackAnswer.push(stackPN.peek());
            }else {
                double buf = 0;
                switch (stackPN.peek()) {
                    case "+":
                        buf = Double.parseDouble(stackAnswer.pop())
                                + Double.parseDouble(stackAnswer.pop());
                        stackAnswer.push(String.valueOf(buf));

                        break;
                    case "-":
                        buf = -1 * Double.parseDouble(stackAnswer.pop()) + Double.parseDouble(stackAnswer.pop());
                        stackAnswer.push(String.valueOf(buf));

                        break;
                    case "*":
                        buf = Double.parseDouble(stackAnswer.pop()) * Double.parseDouble(stackAnswer.pop());
                        stackAnswer.push(String.valueOf(buf));

                        break;
                    case "/":
                        double p = Double.parseDouble(stackAnswer.pop());
                        double q = Double.parseDouble(stackAnswer.pop());
                        // обрабатываем деление на 0
                        if(p == 0) {
                            throw new EmptyStackException();
                        }else {
                            stackAnswer.push(String.valueOf(q/p));
                        }
                        break;
                    default:
                        return null;
                }
            }
            stackPN.pop();
        }
        return stackAnswer.peek();
    }

    public Stack<String> getStackOPN() {
        return stackOPN;
    }
}
